import to from '../../utils/to.js';
import service from './service.js';
import responseController from '../../utils/responseController.js';

const getUser = async (req, res) => {
  try {
    console.log(1)
    const [error, data] = await to(service.getUsers());

    responseController({ error, data }, res);
  } catch (error) {
    throw new Error(error);
  }
};

const register = async (req, res) => {
  try {
    const [error, data] = await to(service.register(req.body));
    responseController({ error, data }, res);
  } catch (error) {
    throw new Error(error);
  }
};

export default {
  getUser,
  register,
};
