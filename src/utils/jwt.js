import generateJwt from 'jsonwebtoken';
import configEnv from '../env/index.js';

function generate(data, option = { expiresIn: configEnv.jwt.expiresIn }) {
  return generateJwt.sign(data, configEnv.jwt.secret, option);
}

function verifyToken(token) {
  const verifyTokenResult = generateJwt.verify(token, configEnv.jwt.secret);

  return verifyTokenResult;
}

export default {
  generate,
  verifyToken,
};
