export const buildFindingQuery = (query) => {
  let {
    page, limit, skip, sortType, sortBy, ...search
  } = query;

  page = query.page || 0;
  limit = query.limit || 20;
  skip = limit * page;
  sortType = Number(query.sortType) || -1;
  sortBy = query.sortBy || 'createdAt';

  return {
    page: parseInt(page, 10),
    limit: parseInt(limit, 10),
    skip,
    sort: { [sortBy]: sortType },
    sortType,
    sortBy,
    search,
  };
};

export const paginateValidateDefault = {

};
