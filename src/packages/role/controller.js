import to from '../../utils/to.js';
import service from './service.js';
import responseController from '../../utils/responseController.js';

const getManyRole = async (req, res) => {
  try {
    console.log(1)
    const [error, data] = await to(service.getManyRole());

    responseController({ error, data }, res);
  } catch (error) {
    throw new Error(error);
  }
};

const create = async (req, res) => {
  try {
    const [error, data] = await to(service.create(req.body));
    responseController({ error, data }, res);
  } catch (error) {
    throw new Error(error);
  }
};

export default {
  getManyRole,
  create,
};
