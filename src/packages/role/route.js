import express from 'express';
import controller from './controller.js';
import paramValidator from '../../middlewares/validator.js'

export const router = express.Router();
router.post(
  '/add',
  controller.create,
);

router.get(
  '',
  controller.getManyRole,
);

export default router;
