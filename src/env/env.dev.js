import {} from 'dotenv/config';

export default {
  db: {
    host: process.env.SEQUELIZE_HOST,
    user: process.env.SEQUELIZE_USER,
    password: process.env.SEQUELIZE_PASSWORD,
    dbName: process.env.SEQUELIZE_DB_NAME,
    dialect: process.env.SEQUELIZE_DIALECT,
    pool: {
      max: Number(process.env.SEQUELIZE_POOL_MAX),
      min: Number(process.env.SEQUELIZE_POOL_MIN),
      acquire: Number(process.env.SEQUELIZE_POOL_ACQUIRE),
      idle: Number(process.env.SEQUELIZE_POOL_IDLE),
    },
  },
  nodeEnv: process.env.NODE_ENV,
  jwt: {
    secret: process.env.JWT_SCERET,
    expiresIn: process.env.JWT_EXPIRES_IN,
  },
  crypto: {
    secret: process.env.SECRET,
  },
  google: {
    clientID: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: process.env.GOOGLE_CALLBACK,
  },
  github: {
    clientID: process.env.GITHUB_CLIENT_ID,
    clientSecret: process.env.GITHUB_CLIENT_SECRET,
  },
  host: {
    feHost: process.env.FE_HOST,
    beHost: process.env.BE_HOST,
  },
  bcrypt: {
    saltRounds: Number(process.env.SALT_ROUNDS),
  },
  sendGrid: {
    apiKey: process.env.SEND_GRID_API_KEY,
  },
};
