import express from 'express';
import bodyParser from 'body-parser';
import swaggerUi from 'swagger-ui-express';
import swaggerJsDoc from 'swagger-jsdoc';
import { ValidationError } from 'express-validation';
import dotenv from 'dotenv';
import winston from 'winston'
import route from './routes/index.js';
import configEnv from './env/index.js';
import connectDB from './database/connect.js';

const bootstrap = async () => {
  const app = express();
  dotenv.config();
  const port = 3000;

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));

  await connectDB();

  app.use('/', route());

  app.get('/customers', (req, res) => {
    res.status(200).send('Customer results');
  });

  const swaggerOptions = {
    swaggerDefinition: {
      info: {
        version: '1.0.0',
        title: 'Customer API',
        description: 'Customer API Information',
        contact: {
          name: 'Amazing Developer',
        },
        servers: ['http://localhost:3000'],
      },
    },
    // ['.routes/*.js']
    apis: ['src/packages/*/route.js'],
  };

  const swaggerDocs = swaggerJsDoc(swaggerOptions);
  app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));

  app.use(handleNotFoundError);
  app.use((err, req, res, next) => {
    const detailsError = err?.details?.body?.length
      ? err?.details?.body[0] : err?.details?.query[0]

    if (err instanceof ValidationError) {
      return res.status(err.statusCode)
        .json({
          name: err.name,
          code: err.statusCode,
          message: detailsError?.message?.replace(/\"/g, ''),
        });
    }

    return res.status(500).json(err);
  });

  app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
    console.log(`Example app running on ${configEnv.nodeEnv}`);
  });
};

function handleNotFoundError(req, res) {
  console.log('404', req.url);
  winston.info({
    data: {
      url: `404 - ${req.method.toUpperCase()} ${req.url}`,
      clientData: ['get', 'delete'].includes(req.method.toLowerCase())
        ? req.query
        : req.body,
    },
  });

  return res.status(404).jsonp(
    {
      url: `404 - ${req.method.toUpperCase()} ${req.url}`,
      clientData: ['get', 'delete'].includes(req.method.toLowerCase())
        ? req.query
        : req.body,
    },
  );
}

bootstrap();
