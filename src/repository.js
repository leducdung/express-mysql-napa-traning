const Repository = (Model) => {
  const repositories = Object.freeze({
    create: async (data) => {
      const created = await Model.create(data)

      return created
    },
    findById: async (id) => {
      const findById = await Model.findByPk(id)

      return findById
    },
    findAll: async (query) => {
      const findAll = await Model.findAll(query)

      return findAll
    },
    removeById: async (id) => {
      const deleted = await Model.destroy({
        where: { id },
      })

      return deleted
    },
    updateById: async (data, id) => {
      const updateData = await Model.update(data, {
        where: { id },
      })

      return updateData
    },
  });

  return repositories
}

export default Repository
