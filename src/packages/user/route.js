import express from 'express';
import controller from './controller.js';
import paramValidator from '../../middlewares/validator.js'

export const router = express.Router();

/**
 * @swagger
 * /books/{id}:
 *   get:
 *     summary: Get the book by id
 *     tags: [Books]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The book id
 *     responses:
 *       200:
 *         description: The book description by id
 *         contens:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Book'
 *       404:
 *         description: The book was not found
 */

router.post(
  '/register',
  paramValidator.user.register,
  controller.register,
);

router.get(
  '',
  controller.getUser,
);

export default router;
