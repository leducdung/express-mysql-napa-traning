import { Sequelize } from 'sequelize';
import configEnv from '../env/index.js';

const sequelize = new Sequelize(
  configEnv.db.dbName,
  configEnv.db.user,
  configEnv.db.password,
  {
    host: configEnv.db.host,
    dialect: 'mysql',
    operatorsAliases: false,
    pool: {
      max: configEnv.db.pool.max,
      min: configEnv.db.pool.min,
      acquire: configEnv.db.pool.acquire,
      idle: configEnv.db.pool.idle,
    },
    retry: {
      match: [
        /SequelizeConnectionError/,
        /SequelizeConnectionRefusedError/,
        /SequelizeHostNotFoundError/,
        /SequelizeHostNotReachableError/,
        /SequelizeInvalidConnectionError/,
        /SequelizeConnectionTimedOutError/
      ],
      name: 'query',
      backoffBase: 100,
      backoffExponent: 1.1,
      timeout: 60000,
      max: 3,
    }
  },
);

export default sequelize;
