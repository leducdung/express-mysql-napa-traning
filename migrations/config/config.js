import config from 'my/app/config';

const env = process.env.NODE_ENV || 'development';

export default {
  [env]: {
    username: 'root',
    password: '14091999',
    database: 'node_sequelize_api_db',
    host: '127.0.0.1',
    dialect: 'mysql'
  }
};
