import { User } from '../../models.js';
import Repository from '../../repository'

const getUsers = async () => {
  try {
    await Repository(User).create({ firstName: 'Jane', lastName: 'Doe', username: '13231', password: '1234124' });
    const users = await Repository(User).findAll();

    return users;
  } catch (err) {
    throw new Error(err.message);
  }
};

const register = async (data) => {
  try {
    const user = await Repository(User).create(data);

    return user;
  } catch (err) {
    throw new Error(err.message);
  }
};

export default {
  getUsers,
  register,
};
