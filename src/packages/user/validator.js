import Joi from 'joi';

export default {
  register: {
    body: Joi.object({
      username: Joi.string().required(),
      password: Joi.string()
        .min(8)
        .regex(/(?=.*[A-Z])(?=.*\d)(?=.*[!"#$%&'()*\+,-.\/\\:;<=>?@[\]^_`{|}~])/)
        .message('Password must have at least 1 number, 1 uppercase and 1 special characters')
        .required(),
      firstName: Joi.string().required(),
      lastName: Joi.string().required(),
      email: Joi.string().email().trim().required(),
    }),
  },
};
