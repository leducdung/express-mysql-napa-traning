export default ({ error, data }, res) => {
  if (!error) {
    return res.send({ error, data });
  }

  if (error.code) {
    return res.status(error.code).json(error);
  }

  return res.status(500).json({
    error,
    message: error.message,
    stack: error.stack,
  });
};
