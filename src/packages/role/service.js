import { User, Role } from '../../models.js';

const getManyRole = async () => {
  try {
    const roles = await Role.findAll({
      include: User,
    });

    return roles;
  } catch (err) {
    throw new Error(err.message);
  }
};

const create = async (data) => {
  try {
    const user = await Role.create({
      userId: '2364a3cb-e532-408e-a412-d4add8ba5c9f',
    });

    return user;
  } catch (err) {
    throw new Error(err.message);
  }
};

export default {
  getManyRole,
  create,
};
