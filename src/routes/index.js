import { Router } from 'express';
import userRoute from '../packages/user/route.js';
import roleRoute from '../packages/role/route.js';

export default () => {
  const api = Router();
  api.use('/users', userRoute);
  api.use('/roles', roleRoute);
  /**
   * @swagger
   * /getUser:
   *   get:
   *     description: Get all books
   *     responses:
   *       200:
   *         description: Success
   */
  return api;
};
