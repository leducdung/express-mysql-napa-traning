import { Sequelize, DataTypes } from 'sequelize';
import sequelize from './database/index.js';
import UserModel from './packages/user/model.js';
import RoleModel from './packages/role/model.js';

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

const User = UserModel(sequelize, DataTypes);

const Role = RoleModel(sequelize, DataTypes);
Role.belongsTo(User, { foreignKey: 'userId' });

db.sequelize.sync({ force: false })
  .then(() => {
    console.log('yes re-sync done!');
  });

export {
  User,
  Role,
};
